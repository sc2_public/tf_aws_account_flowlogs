data "aws_organizations_organization" "main" {}

# FlowLogs bucket
data "aws_s3_bucket" "flowlogs" {
  bucket   = "${data.aws_organizations_organization.main.id}-flowlogs"
  provider = aws.audit
}

# get VPCs in this region

data "aws_vpcs" "vpcs" {
}

