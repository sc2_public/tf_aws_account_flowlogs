# configure VPC Flow Logs

provider "aws" {
  alias = "audit"
}

# create a flow log for each VPC

resource "aws_flow_log" "org" {
  for_each             = data.aws_vpcs.vpcs.ids
  vpc_id               = each.key
  traffic_type         = "ALL"

  log_destination      = data.aws_s3_bucket.flowlogs.arn
  log_destination_type = "s3"

  tags = {
    Name    = "flow-log-${each.key}"
    vpc     = each.key
    creator = "org"
  }
}

