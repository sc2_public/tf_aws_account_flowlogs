# tf_aws_account_flowlogs

Enables VPC Flow Logs for all VPCs in a Region

* Enables flow logging to a shared S3 bucket for all traffic in every VPC within the region

## Variables

None.

## Outputs

None.

## Usage

This module must be instantiated for each region that requires VPC Flow Logging.

```terraform
module "account" {
  source = "git::https://code.stanford.edu/sc2_public/tf_aws_account_default.git"
  alias  = "foo-prod"
}

module "flowlogs_us_west_2" {
  source      = "git::https://code.stanford.edu/sc2_public/tf_aws_account_flowlogs.git"
  providers   = {
    aws       = aws.us_west_2
    aws.audit = aws.audit
  }
}
```

## Related Modules

* [AWS Default Setup](https://code.stanford.edu/sc2_public/tf_aws_account_default/)
* [VPC FlowLogs setup](https://code.stanford.edu/sc2_public/tf_aws_account_flowlogs)
* [Guardduty setup](https://code.stanford.edu/sc2_public/tf_aws_account_guardduty)
